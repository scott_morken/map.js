var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var vfs = require('vinyl-fs');
var concat = require('gulp-concat');
var gutil = require('gulp-util');
var chmod = require('gulp-chmod');

gulp.task('default', ['js', 'hook']);

gulp.task('js', function () {
    gulp.src([
        './src/async.js',
        './src/map.js',
        './src/marker.js',
        './src/position.js'
    ])
        .pipe(concat('map.js'))
        .pipe(gulp.dest('./dist'))
        .pipe(uglify())
        .pipe(rename('map.min.js'))
        .pipe(gulp.dest('./dist'))
        .on('error', gutil.log);
});

gulp.task('hook', function () {
    return vfs.src('.pre-commit')
        .pipe(chmod(0o755))
        .pipe(rename('pre-commit'))
        .pipe(vfs.dest('.git/hooks', {overwrite: true}));
});
