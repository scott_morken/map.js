/**
 * Created by smorken on 4/21/15.
 */
(function(wHnd) {
    smorken.Extend('smorken.map.Async', {
        status: {},
        api_key: null,
        init: function (api_key) {
            this.api_key = api_key;
            this.add(this.loaders);
        },
        add: function (loaders) {
            smorken.each(loaders, function (i, func) {
                smorken.map.Async.status[i] = false;
                smorken.Event.trigger(window, 'loading:' + i);
                func();
            });
        },
        addTags: function(src) {
            var script_tag = document.createElement('script');
            script_tag.setAttribute("type", "text/javascript");
            script_tag.setAttribute("src", src);
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
        },
        loaders: {
            gmap: function () {
                var url = "//maps.google.com/maps/api/js?v=3&sensor=false&callback=smorken.map.Async.loaded.gmap";
                if (smorken.map.Async.api_key) {
                    url += "&key=" + smorken.map.Async.api_key;
                }
                smorken.map.Async.addTags(url);
            }
        },
        loaded: {
            gmap: function () {
                smorken.map.Async.status.gmap = true;
                smorken.Event.trigger(window, 'loaded:gmap');
            }

        }
    });
})(window);
