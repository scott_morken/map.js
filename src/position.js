/**
 * Created by smorken on 4/21/15.
 */
(function (wHnd) {
    var position = {
        watchid: null,
        nav: navigator.geolocation || false,
        watch: function () {
            if (this.watchid) {
                this.nav.clearWatch(this.watchid);
            }
            if (this.nav) {
                this.watchid = this.nav.watchPosition(
                    this.success,
                    this.failure,
                    {
                        enableHighAccuracy: true,
                        timeout: 27000
                    }
                );
            }
        },
        getCurrentPosition: function () {
            if (this.nav) {
                this.nav.getCurrentPosition(this.success, this.failure);
            }
        },
        success: function (position) {
            smorken.Event.trigger(window, 'position:change', {position: position});
        },
        failure: function (error) {
            smorken.Event.trigger(window, 'position:error', {code: error.code});
        }
    };
    smorken.Extend('smorken.map.Position', position);
})(window);
