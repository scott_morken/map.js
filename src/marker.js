/**
 * Created by smorken on 4/21/15.
 */
(function(wHnd) {
    var marker = function (map, latLong, icon, info) {
        this.map = map;
        this.latLong = latLong;
        this.icon = icon;
        this.info = info || null;
        this.marker = null;


        this.update(latLong, info);
    };
    marker.prototype = {
        update: function (latLong, info) {
            if (latLong) {
                if (latLong.coords) {
                    latLong = new google.maps.LatLng(latLong.coords.latitude, latLong.coords.longitude);
                }
                this.latLong = latLong;
                this.info = info || this.info;
                if (this.marker) {
                    this.marker.setPosition(latLong);
                }
                else {
                    this.marker = new google.maps.Marker({
                        position: this.latLong,
                        map: this.map,
                        icon: this.icon,
                        zIndex: 999,
                        clickable: false
                    });
                }
                this.addInfo(info);
            }
        },
        addInfo: function (info) {
            if (info) {
                var infoWindow = new google.maps.InfoWindow({
                    content: '<span>' + info + '</span>'
                });
                infoWindow.open(this.map, this.marker);
            }
        }
    };
    marker.prototype.constructor = marker;
    smorken.Extend('smorken.map.Marker', marker);
})(window);
