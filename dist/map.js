/**
 * Created by smorken on 4/21/15.
 */
(function(wHnd) {
    smorken.Extend('smorken.map.Async', {
        status: {},
        api_key: null,
        init: function (api_key) {
            this.api_key = api_key;
            this.add(this.loaders);
        },
        add: function (loaders) {
            smorken.each(loaders, function (i, func) {
                smorken.map.Async.status[i] = false;
                smorken.Event.trigger(window, 'loading:' + i);
                func();
            });
        },
        addTags: function(src) {
            var script_tag = document.createElement('script');
            script_tag.setAttribute("type", "text/javascript");
            script_tag.setAttribute("src", src);
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
        },
        loaders: {
            gmap: function () {
                var url = "//maps.google.com/maps/api/js?v=3&sensor=false&callback=smorken.map.Async.loaded.gmap";
                if (smorken.map.Async.api_key) {
                    url += "&key=" + smorken.map.Async.api_key;
                }
                smorken.map.Async.addTags(url);
            }
        },
        loaded: {
            gmap: function () {
                smorken.map.Async.status.gmap = true;
                smorken.Event.trigger(window, 'loaded:gmap');
            }

        }
    });
})(window);

/**
 * Created by smorken on 4/21/15.
 */
(function (wHnd) {
    var map = function (options) {
        this.options = options || {};
        this.map = null;
        this.markers = {
            'from': null,
            'to': null
        };
        this.bounds = null;
        this.has_been_fit = false;
        this.on_map_loaded = [
            function (self) {
                smorken.Event.bind(window, 'resize', function () {
                    if (self.map) {
                        var currentCenter = self.map.getCenter();
                        google.maps.event.trigger(self.map, 'resize');
                        self.map.setCenter(currentCenter);
                        self.map.setZoom(self.getZoom());
                    }
                });
            }
        ];
        this.on_map_created = [
            function (self) {
                google.maps.event.addListenerOnce(self.map, 'idle', function () {
                    if (self.options.loading_element) {
                        var els = document.querySelectorAll(self.options.loading_element);
                        for (var i = 0; i < els.length; i++) {
                            els[i].style.display = 'none';
                        }
                    }
                });
            },
            function (self) {
                smorken.Event.bind(window, 'position:change', function (event) {
                    var position = event.data.position;
                    var latLong = self.getLatLong(position.coords.latitude, position.coords.longitude);
                    if (self.markers.from) {
                        self.markers.from.update(latLong);
                    }
                    if (!self.has_been_fit && self.bounds) {
                        self.bounds.extend(latLong);
                        self.map.fitBounds(self.bounds);
                        self.has_been_fit = true;
                    }
                    return true;
                });
            },
            function (self) {
                if (window.DeviceOrientationEvent) {
                    smorken.Event.bind(window, 'deviceorientation', function (event) {
                        if (self.markers.from.marker) {
                            var alpha = null;
                            if (event.webkitCompassHeading) {
                                alpha = event.webkitCompassHeading;
                            }
                            else {
                                alpha = event.alpha;
                            }
                            var icon = self.markers.from.marker.get('icon');
                            icon.rotation = 360 - alpha;
                            self.markers.from.marker.set('icon', icon);
                        }
                        return true;
                    });
                }
            },
            function (self) {
                smorken.Event.bind(window, 'map:redraw', function (event) {
                    if (self.map) {
                        google.maps.event.trigger(self.map, 'resize');
                        if (self.bounds) {
                            self.map.fitBounds(self.bounds);
                        }
                        else {
                            var currentCenter = self.map.getCenter();
                            self.map.setCenter(currentCenter);
                            self.map.setZoom(self.getZoom());
                        }
                    }
                });
            }
        ];
        this.loadMap();
    };
    map.prototype = {
        create: function (mapElement, lat, long, name, nav) {
            var self = this;
            if (smorken.map.Async.status.gmap) {
                nav = nav || false;
                var latLong = this.getLatLong(lat, long);
                this.has_been_fit = false;
                this.bounds = new google.maps.LatLngBounds();
                this.bounds.extend(latLong);
                var options = {
                    center: this.getCenter(latLong),
                    zoom: this.getZoom(),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                this.map = this.map || new google.maps.Map(mapElement, options);
                this.addLoaders(this.on_map_created);
                var icon = {
                    path: google.maps.SymbolPath.CIRCLE,
                    strokeColor: 'red',
                    scale: 4
                };
                this.markers.to = new smorken.map.Marker(this.map, latLong, icon, name);
                if (nav) {
                    this.addNav();
                }
            }
            else {
                smorken.Event.bind(window, 'loaded:gmap', function () {
                    self.create(mapElement, lat, long, name, nav);
                });
            }
        },
        addNav: function () {
            var navPos = smorken.map.Position;
            var navIcon = {
                path: google.maps.SymbolPath.CIRCLE,
                strokeColor: '#3333FF',
                strokeWeight: 5,
                scale: 2.5
            };
            this.markers.from = new smorken.map.Marker(this.map, null, navIcon);
            navPos.watch();
        },
        loadMap: function () {
            if (!smorken.map.Async.status.gmap) {
                var self = this;
                smorken.Event.bind(window, 'loaded:gmap', function () {
                    self.addLoaders(self.on_map_loaded);
                });
                smorken.map.Async.init(this.getOption('api_key'));
            }
        },
        addLoaders: function (loaders) {
            if (!this.initialized) {
                var self = this;
                smorken.each(loaders, function (i, func) {
                    func(self);
                });
            }
        },
        getZoom: function () {
            return 17;
        },
        getCenter: function (latLong, latLong2) {
            return latLong;
        },
        getLatLong: function (lat, long) {
            return new google.maps.LatLng(lat, long);
        },
        getOption: function (key) {
            if (this.options[key]) {
                return this.options[key];
            }
            return null;
        }
    };
    map.prototype.constructor = map;
    smorken.Extend('smorken.map.Map', map);
})(window);

/**
 * Created by smorken on 4/21/15.
 */
(function(wHnd) {
    var marker = function (map, latLong, icon, info) {
        this.map = map;
        this.latLong = latLong;
        this.icon = icon;
        this.info = info || null;
        this.marker = null;


        this.update(latLong, info);
    };
    marker.prototype = {
        update: function (latLong, info) {
            if (latLong) {
                if (latLong.coords) {
                    latLong = new google.maps.LatLng(latLong.coords.latitude, latLong.coords.longitude);
                }
                this.latLong = latLong;
                this.info = info || this.info;
                if (this.marker) {
                    this.marker.setPosition(latLong);
                }
                else {
                    this.marker = new google.maps.Marker({
                        position: this.latLong,
                        map: this.map,
                        icon: this.icon,
                        zIndex: 999,
                        clickable: false
                    });
                }
                this.addInfo(info);
            }
        },
        addInfo: function (info) {
            if (info) {
                var infoWindow = new google.maps.InfoWindow({
                    content: '<span>' + info + '</span>'
                });
                infoWindow.open(this.map, this.marker);
            }
        }
    };
    marker.prototype.constructor = marker;
    smorken.Extend('smorken.map.Marker', marker);
})(window);

/**
 * Created by smorken on 4/21/15.
 */
(function (wHnd) {
    var position = {
        watchid: null,
        nav: navigator.geolocation || false,
        watch: function () {
            if (this.watchid) {
                this.nav.clearWatch(this.watchid);
            }
            if (this.nav) {
                this.watchid = this.nav.watchPosition(
                    this.success,
                    this.failure,
                    {
                        enableHighAccuracy: true,
                        timeout: 27000
                    }
                );
            }
        },
        getCurrentPosition: function () {
            if (this.nav) {
                this.nav.getCurrentPosition(this.success, this.failure);
            }
        },
        success: function (position) {
            smorken.Event.trigger(window, 'position:change', {position: position});
        },
        failure: function (error) {
            smorken.Event.trigger(window, 'position:error', {code: error.code});
        }
    };
    smorken.Extend('smorken.map.Position', position);
})(window);
