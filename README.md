### NPM Install

```
{
  "name": "my-project",
  "dependencies": {
    "map.js": "git+https://bitbucket.org/scott_morken/map.js.git"
  }
}
```

### Gulpfile addition

```
...
  .copy('./node_modules/smorken.js/dist', './public/js/limited')
  .copy('./node_modules/map.js/dist', './public/js/limited')
...
```
